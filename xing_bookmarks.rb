begin
    require 'xing_api'
    require 'launchy'
    require 'rubygems'
    require 'write_xlsx'
    require "date"
    require "open-uri"
    require "fileutils"
    
    #Definiere Dev keys
    consumer_key = "e24d1c824caa97fcc7ac"
    consumer_secret = "a342a19e7756ea2ac9089b23418575259a5ffed8"

    client = XingApi::Client.new(
        consumer_key: consumer_key,
        consumer_secret: consumer_secret
    )

    # Step 1: Obtain a request token
    puts "\nStarting oauth handshake, ask for request token..."
    request_token = client.get_request_token

    # Step 2: Obtain user authorization
    print "Opening browser for authentication, please login..."
    Launchy.open(request_token[:authorize_url])
    print "\nPlease enter the PIN here: "
    verifier = gets.strip

    # Step 3: Exchange the authorized request token for an access token
    # more info -> https://dev.xing.com/docs/authentication#call_v1_access_token
    puts "\nExchanging request token for your access token..."
    access_token = client.get_access_token(verifier)
    #puts " access token: #{access_token[:access_token]}"
    #puts " access token secret: #{access_token[:access_token_secret]}"
      
    puts "\nGetting your Bookmarks..."
    begin
        response = XingApi::Bookmark.list(client: client)[:bookmarks]
    rescue XingApi::Error => e
        puts "Error!"
        puts "Code:\t"+(e.status_code).to_s
        puts "Name:\t"+e.name
        puts "Text:\t"+e.text
        puts "Errors:\t"+(e.errors).to_s
    end
    


    d = DateTime.now()
    filename = "bookmarks_" + (d.strftime("%m.%d.%Y_%H.%M")).to_s + ".xlsx"
    if File.exist?(filename)
        print "File \"" + filename + "\" already exists!"
        exit
    else   
        workbook = WriteXLSX.new(filename)
        format = workbook.add_format(:halign => 1)
        hyperlink_format = workbook.add_format(
                     :color     => 'blue',
                     :underline => 1
        )
        worksheet = workbook.add_worksheet
        
        col = 0
        row = 0
        
        #print "Total amount of Bookmarks:\t" + response[:total].to_s + "\n"
        worksheet.write(row, col, "Bookmarks: " + response[:total].to_s)
        worksheet.write(row+2, col, "created at")
        worksheet.set_column(col, col, 20)
        worksheet.write(row+2, col+1, "first name")
        worksheet.set_column(col+1, col+1, 12)
        worksheet.write(row+2, col+2, "last name")
        worksheet.set_column(col+2, col+2, 12)
        worksheet.write(row+2, col+3, "photo")
        worksheet.set_column(col+3, col+3, 20)
        worksheet.write(row+2, col+4, "birth date")
        worksheet.set_column(col+4, col+4, 10)
        worksheet.write(row+2, col+5, "mail")
        worksheet.set_column(col+5, col+5, 45)
        worksheet.write(row+2, col+6, "xing link")
        worksheet.set_column(col+6, col+6, 50)
        response = response[:items]
        response.each_index{|x|
            #puts x.to_s + " | " + (response[x][:user][:id]).to_s + "\tcreated at: " + (response[x][:created_at]).to_s
            created_at = (response[x][:created_at]).to_s
            created_at = created_at.chop
            created_at = created_at.split("T")
            created_at_date = created_at[0]
            created_at_time = created_at[1]
            worksheet.write(x+3, col, created_at_date + " " + created_at_time)
            id = response[x][:user][:id]
            begin
                user = XingApi::User.find(id, client: client)[:users]
                #puts user[0][:first_name] + " " + user[0][:last_name] + " | Xing-Link: " + user[0][:permalink]
                worksheet.write(x+3, col+1, user[0][:first_name])
                worksheet.write(x+3, col+2, user[0][:last_name])
                #permalink = "=HYPERLINK(\"" + user[0][:permalink] + "\";\"LINK\")"
                permalink = "=HYPERLINK(\"" + user[0][:permalink]+ "\")"
                #puts permalink
                worksheet.write(x+3, col+6, permalink, hyperlink_format)
                
                #puts user[0][:birth_date][:day].to_s + "." + user[0][:birth_date][:month].to_s + "." + user[0][:birth_date][:year].to_s
                worksheet.write(x+3, col+4, user[0][:birth_date][:day].to_s + "." + user[0][:birth_date][:month].to_s + "." + user[0][:birth_date][:year].to_s)
                
                #puts "Mail: " + user[0][:active_email] + " - photo url: " + user[0][:photo_urls][:size_96x96]
                #mail = "=HYPERLINK(\"" + user[0][:active_email] + "\";\"Mail\")"
                mail = "=HYPERLINK(\"" + user[0][:active_email] + "\")"
                worksheet.write(x+3, col+5, mail, hyperlink_format)

                
                
                #image_path = user[0][:photo_urls][:size_96x96]
                worksheet.write(x+3, col+3, image_path)
                
                #C:\Users\weg>bitsadmin /transfer whatsoever /download /priority normal https://www.xing.com/assets/frontend_minified/img/users/nobody_m.96x96.jpg C:\Users\weg\Desktop\test.jpg
                
                puts image_path
                #File.open("tmp_image","wb+") do |file|
                #    file.write open(image_path).read
                #end       
                           
                
                #worksheet.insert_image(x+3, col+3, "tmp_image")
                
            rescue XingApi::Error => e
                puts "Error!"
                puts "Code:\t"+(e.status_code).to_s
                puts "Name:\t"+e.name
                puts "Text:\t"+e.text
                puts "Errors:\t"+(e.errors).to_s
            end
        }
        workbook.close
        #File.delete "tmp_image"
    end
end